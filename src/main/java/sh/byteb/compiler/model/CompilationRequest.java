package sh.byteb.compiler.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;


public class CompilationRequest {
    private final static int DEFAULT_MEMORY = 500;
    private final static int DEFAULT_RUNTIME = 2000;
    private final static String INVALID_MEMORY_VALUE = "Invalid max memory value";
    private final static String INVALID_RUNTIME_VALUE = "Invalid value for max runtime";
    private final static Language DEFAULT_LANGUAGE = Language.CPP;


    @JsonProperty("language")
    Language language;

    @JsonProperty("maxMemory")
    @Min(value = 100, message = INVALID_MEMORY_VALUE)
    @Max(value = 2048, message = INVALID_MEMORY_VALUE)
    int maxMemory;

    @JsonProperty("timeLimit")
    @Min(value = 100, message = INVALID_RUNTIME_VALUE)
    @Max(value = 5000, message = INVALID_RUNTIME_VALUE)
    int timeLimit;

    CompilationRequest(Language language, int maxMemory, int timeLimit) {
        this.language = language;
        this.maxMemory = maxMemory;
        this.timeLimit = timeLimit;
    }

    CompilationRequest(Language language) {
        this(language, DEFAULT_MEMORY, DEFAULT_RUNTIME);
    }

    CompilationRequest() {
        this(DEFAULT_LANGUAGE, DEFAULT_MEMORY, DEFAULT_RUNTIME);
    }


    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public int getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(int maxMemory) {
        this.maxMemory = maxMemory;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

}
