package sh.byteb.compiler.model;

public class CompilationResponse {

    public CompilationResponse(String id){
        this.id = id;
    }

    String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
