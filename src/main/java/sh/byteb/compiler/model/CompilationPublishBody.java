package sh.byteb.compiler.model;

import java.util.UUID;

public class CompilationPublishBody {

    private final CompilationRequest compilationRequest;
    private final String stdin;
    private final String code;


    private final String uuid;

    public CompilationPublishBody(CompilationRequest compilationRequest, String stdin, String code, String uuid) {
        this.compilationRequest = compilationRequest;
        this.stdin = stdin;
        this.code = code;
        this.uuid = uuid;
    }

    public CompilationPublishBody(CompilationRequest compilationRequest, String stdin, String code) {
        this(compilationRequest, stdin, code, UUID.randomUUID().toString());
    }

    public CompilationRequest getCompilationRequest() {
        return compilationRequest;
    }

    public String getStdin() {
        return stdin;
    }

    public String getCode() {
        return code;
    }

    public String getUuid() {
        return uuid;
    }

}
