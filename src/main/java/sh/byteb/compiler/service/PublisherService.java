package sh.byteb.compiler.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sh.byteb.compiler.model.CompilationPublishBody;
import sh.byteb.compiler.model.CompilationRequest;
import sh.byteb.compiler.publisher.Publisher;

import java.util.UUID;

@Service("publisherService")
public class PublisherService {

    private final Publisher rabbitMqPublisher;

    public PublisherService(@Qualifier("rabbitMqPublisher") Publisher rabbitMqPublisher) {
        this.rabbitMqPublisher = rabbitMqPublisher;
    }


    public String publishToQueue(CompilationRequest reqBody, String stdin, String code) {
        var submissionId = UUID.randomUUID().toString();
        rabbitMqPublisher.publish(new CompilationPublishBody(reqBody, stdin, code, submissionId));
        return submissionId;
    }

}
