package sh.byteb.compiler.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sh.byteb.compiler.model.CompilationRequest;
import sh.byteb.compiler.model.CompilationResponse;
import sh.byteb.compiler.service.PublisherService;


@RestController
public class CompilerController {
    Logger log = LoggerFactory.getLogger(CompilerController.class);

    PublisherService service;

    public CompilerController(PublisherService service) {
        this.service = service;
    }

    @PostMapping("/")
    ResponseEntity<CompilationResponse> runRequest(@RequestParam(value = "metadata") CompilationRequest request, @RequestParam(value = "stdin") String stdin, @RequestParam(value = "code") String code

    ) {
        return ResponseEntity.status(202).body((new CompilationResponse(service.publishToQueue(request, stdin, code))));
    }

}
