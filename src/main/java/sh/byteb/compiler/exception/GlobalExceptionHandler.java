package sh.byteb.compiler.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.net.URISyntaxException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InternalRuntimeException.class)
    ProblemDetail handleInternalRuntimeException(HttpServletRequest request, InternalRuntimeException exception) throws URISyntaxException {
        var pd = ProblemDetail.forStatus(exception.getResponseCode());
        pd.setDetail(exception.getReason());
        pd.setTitle(exception.getTitle());
        pd.setInstance(new URI(request.getRequestURI()));
        return pd;
    }

}
