package sh.byteb.compiler.publisher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import sh.byteb.compiler.exception.InternalRuntimeException;
import sh.byteb.compiler.model.CompilationPublishBody;


@Component("rabbitMqPublisher")
public class Publisher {

    Logger log = LoggerFactory.getLogger(Publisher.class);

    private final RabbitTemplate rabbitTemplate;

    private final ObjectMapper objectMapper;


    public Publisher(RabbitTemplate rabbitTemplate, ObjectMapper mapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = mapper;
    }

    public void publish(CompilationPublishBody publishBody) {
        var message = MessageBuilder.withBody(serializeToByteArray(publishBody))
                .setHeader("language", publishBody.getCompilationRequest().getLanguage().name())
                .build();

        rabbitTemplate.send(
                "sh.byteb.compiler",
                publishBody.getCompilationRequest().getLanguage().name().toLowerCase(),
                message
        );
    }


    private byte[] serializeToByteArray(CompilationPublishBody body) {
        try {
            return objectMapper.writeValueAsBytes(body);
        } catch (JsonProcessingException e) {
            log.error("Exception occurred while trying to serialize body before publishing : ", e);
            throw new InternalRuntimeException("Internal Server Error", "Unable to serialize request", 500);
        }
    }

}
