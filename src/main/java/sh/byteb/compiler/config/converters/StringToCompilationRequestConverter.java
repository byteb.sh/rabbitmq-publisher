package sh.byteb.compiler.config.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import sh.byteb.compiler.model.CompilationRequest;


public class StringToCompilationRequestConverter implements Converter<String, CompilationRequest> {

    Logger log = LoggerFactory.getLogger(StringToCompilationRequestConverter.class);

    final private ObjectMapper objectMapper;

    public StringToCompilationRequestConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    @Override
    public CompilationRequest convert(String source) {
        try {
            return objectMapper.readValue(source, CompilationRequest.class);
        } catch (JsonProcessingException e) {
            log.info("Invalid Request body. Error message : {}", e.getMessage());
            log.debug("Invalid body : {}", source);
            throw new RuntimeException(e);
        }
    }
}
