package sh.byteb.compiler.config.converters;

import org.springframework.core.convert.converter.Converter;
import sh.byteb.compiler.model.Language;

public class StringToLanguageConverter implements Converter<String, Language> {

    @Override
    public Language convert(String source) {
        source = source.toLowerCase();
        return Language.valueOf(source);
    }
}
