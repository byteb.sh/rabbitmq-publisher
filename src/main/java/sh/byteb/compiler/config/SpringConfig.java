package sh.byteb.compiler.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import sh.byteb.compiler.config.converters.StringToCompilationRequestConverter;
import sh.byteb.compiler.config.converters.StringToLanguageConverter;

@Configuration
public class SpringConfig implements WebMvcConfigurer {


    private final ObjectMapper objectMapper;

    SpringConfig(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        WebMvcConfigurer.super.addFormatters(registry);
        registry.addConverter(new StringToCompilationRequestConverter(objectMapper));
        registry.addConverter(new StringToLanguageConverter());
    }


}
