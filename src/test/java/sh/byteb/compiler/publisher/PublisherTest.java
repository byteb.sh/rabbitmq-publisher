package sh.byteb.compiler.publisher;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.RabbitMQContainer;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class PublisherTest {

    @Autowired
    Publisher publisher;

    @Autowired
    RabbitTemplate rabbitTemplate;

    static RabbitMQContainer rabbitMq =
            new RabbitMQContainer("rabbitmq:3-management");

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.username", rabbitMq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitMq::getAdminPassword);
        registry.add("spring.rabbitmq.port", rabbitMq::getAmqpPort);
        registry.add("spring.rabbitmq.host", rabbitMq::getHost);
    }

    @BeforeAll
    static void setUp() {
        //Starting container
        rabbitMq.start();
    }

    @AfterAll
    static void tearDown() {
        rabbitMq.stop();
    }


    @Test
    void publishTest() {

        var rabbitAdmin = new RabbitAdmin(rabbitTemplate.getConnectionFactory());

        assertDoesNotThrow(() -> {
            var executionResult = rabbitMq.execInContainer("rabbitmqctl", "list_queues");
            assertEquals(0, executionResult.getExitCode());
            assertTrue(executionResult.toString().contains("java"));
            assertTrue(rabbitAdmin.getQueueInfo("java").getMessageCount() > 0, "queue should have more than 1 messages");
        });

    }
}